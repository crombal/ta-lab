# module "vault-client" {
#   source = "../modules/ec2"

#   name = "vault-client"

#   ami           = data.aws_ami.ubuntu.id
#   instance_type = "t2.micro"
#   key_name      = data.aws_key_pair.this.key_name
#   monitoring    = false
#   subnet_id     = module.vpc.public_subnets[0]
#   associate_public_ip_address = true

#   user_data_base64 = base64encode(data.template_file.vault-server.rendered)

#   vpc_id = module.vpc.vpc_id

#   ingress_with_cidr_blocks = [for rule in [
#     {
#       rule        = "ssh-tcp"
#       cidr_blocks = "0.0.0.0/0"
#       description = "SHH/TCP on 22 port from all"
#     },
#     {
#       rule        = "vault-tcp"
#       cidr_blocks = "0.0.0.0/0"
#       description = "VAULT/TCP API on 8200 port from all"
#     },
#     {
#       from_port   = 8201
#       to_port     = 8201
#       protocol    = "tcp"
#       cidr_blocks = "0.0.0.0/0"
#       description = "VAULT/TCP Cluster on 8201 port from all"
#     }
#   ] : rule if rule.cidr_blocks != ""]

#   create_iam_instance_profile = true

#   create_route_53_record = false

#   create_alb_rule = false

#   tags = var.tags
# }

# data "template_file" "vault-client" {
#   template = file("./templates/vault-client.tpl")

#   vars = {
#     tpl_vault_zip_file     = local.vault_zip_file
#     tpl_vault_service_name = "vault"
#     tpl_vault_server_addr  = module.vault-server.private_ip
#   }
# }

# resource "aws_iam_role_policy" "vault-client" {
#   name   = "vault-client-role-policy"
#   role   = module.vault-server.iam_role_unique_id
#   policy = data.aws_iam_policy_document.vault-server.json
# }

# data "aws_iam_policy_document" "vault-client" {
#   statement {
#     sid    = "RaftSingle"
#     effect = "Allow"

#     actions = ["ec2:DescribeInstances"]

#     resources = ["*"]
#   }
# }
