module "httpd" {
  source = "../modules/ec2"

  name = "httpd"

  instance_type = "t2.micro"
  key_name      = local.key_pair_name
  monitoring    = false
  subnet_id     = data.aws_ssm_parameter.private_subnet.value

  user_data_base64 = base64encode(join("\n", [
    "#cloud-config",
    yamlencode({
      write_files : [
        {
          path : "/var/www/html/index.html",
          content : "Hello World!"
        },
      ],
      runcmd : [
        ["yum", "update", "-y"],
        ["yum", "install", "-y", "httpd.x86_64"],
        ["systemctl", "start", "httpd.service"],
        ["systemctl", "enable", "httpd.service"],
      ],
    })
  ]))

  vpc_id = data.aws_ssm_parameter.vpc_id.value

  ingress_with_cidr_blocks = [for rule in [
    {
      rule        = "http-80-tcp"
      cidr_blocks = "0.0.0.0/0"
      description = "TCP/HTTP on 80 port from all"
    },
    {
      rule        = "ssh-tcp"
      cidr_blocks = "0.0.0.0/0"
      description = "SHH/TCP on 22 port from all"
    }
  ] : rule if rule.cidr_blocks != ""]

  route53_zone_id = data.aws_ssm_parameter.route53_zone_id.value

  alb_listener_arn = data.aws_ssm_parameter.alb_http_80_listener_arn.value

  target_rules = [
    {
      paths = ["/*"]
      port  = 80
    }
  ]

  tags = var.tags
}