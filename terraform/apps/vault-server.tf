module "vault-server" {
  source = "../modules/ec2"

  name = "vault-server"

  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t2.micro"
  key_name                    = local.key_pair_name
  monitoring                  = false
  subnet_id                   = data.aws_ssm_parameter.public_subnet.value
  associate_public_ip_address = true

  vpc_id = data.aws_ssm_parameter.vpc_id.value

  ingress_with_cidr_blocks = [for rule in [
    {
      rule        = "all-all"
      cidr_blocks = "0.0.0.0/0"
      description = "SHH/TCP on 22 port from all"
    }
  ] : rule if rule.cidr_blocks != ""]

  # ingress_with_cidr_blocks = [for rule in [
  #   {
  #     rule        = "ssh-tcp"
  #     cidr_blocks = "0.0.0.0/0"
  #     description = "SHH/TCP on 22 port from all"
  #   },
  #   {
  #     rule        = "vault-tcp"
  #     cidr_blocks = "0.0.0.0/0"
  #     description = "VAULT/TCP API on 8200 port from all"
  #   },
  #   {
  #     from_port   = 8201
  #     to_port     = 8201
  #     protocol    = "tcp"
  #     cidr_blocks = "0.0.0.0/0"
  #     description = "VAULT/TCP Cluster on 8201 port from all"
  #   }
  # ] : rule if rule.cidr_blocks != ""]

  create_iam_instance_profile = true

  create_route_53_record = true
  route53_zone_id        = data.aws_ssm_parameter.route53_zone_id.value

  create_alb_rule = false

  tags = var.tags
}

resource "aws_iam_role_policy" "vault-server" {
  name   = "vault-server-role-policy"
  role   = module.vault-server.iam_role_name
  policy = data.aws_iam_policy_document.vault-server.json
}

data "aws_iam_policy_document" "vault-server" {
  statement {
    sid    = "RaftSingle"
    effect = "Allow"

    actions = ["ec2:DescribeInstances"]

    resources = ["*"]
  }

  statement {
    sid    = "VaultAWSAuthMethod"
    effect = "Allow"
    actions = [
      "ec2:DescribeInstances",
      "iam:GetInstanceProfile",
      "iam:GetUser",
      "iam:GetRole",
    ]
    resources = ["*"]
  }

  statement {
    sid    = "VaultKMSUnseal"
    effect = "Allow"

    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:DescribeKey",
    ]

    resources = ["*"]
  }
}

resource "aws_kms_key" "vault" {
  description             = "Vault unseal key"
  deletion_window_in_days = 7
}

resource "aws_kms_alias" "vault" {
  name          = "alias/vault-kms-unseal-key"
  target_key_id = aws_kms_key.vault.key_id
}
