provider "aws" {
  region = var.aws_region

  default_tags {
    tags = merge(var.tags, {
      Env     = var.env
      Project = var.project
    })
  }
}
