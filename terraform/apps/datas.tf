data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

data "aws_ssm_parameter" "vpc_id" {
  name            = "/ta-lab/terraform/infra/vpc_id"
  with_decryption = true
}

data "aws_ssm_parameter" "public_subnet" {
  name            = "/ta-lab/terraform/infra/public_subnet"
  with_decryption = true
}

data "aws_ssm_parameter" "private_subnet" {
  name            = "/ta-lab/terraform/infra/private_subnet"
  with_decryption = true
}

data "aws_ssm_parameter" "route53_zone_id" {
  name            = "/ta-lab/terraform/infra/route53_zone_id"
  with_decryption = true
}

data "aws_ssm_parameter" "alb_http_80_listener_arn" {
  name            = "/ta-lab/terraform/infra/alb_http_80_listener_arn"
  with_decryption = true
}
