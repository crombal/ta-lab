# apps

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.3, >= 1.3.7 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 4.52.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.52.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_httpd"></a> [httpd](#module\_httpd) | ../modules/ec2 | n/a |
| <a name="module_vault-server"></a> [vault-server](#module\_vault-server) | ../modules/ec2 | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_iam_role_policy.vault-server](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/iam_role_policy) | resource |
| [aws_kms_alias.vault](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/kms_alias) | resource |
| [aws_kms_key.vault](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/kms_key) | resource |
| [aws_ami.ubuntu](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/data-sources/ami) | data source |
| [aws_iam_policy_document.vault-server](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/data-sources/iam_policy_document) | data source |
| [aws_ssm_parameter.alb_http_80_listener_arn](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/data-sources/ssm_parameter) | data source |
| [aws_ssm_parameter.private_subnet](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/data-sources/ssm_parameter) | data source |
| [aws_ssm_parameter.public_subnet](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/data-sources/ssm_parameter) | data source |
| [aws_ssm_parameter.route53_zone_id](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/data-sources/ssm_parameter) | data source |
| [aws_ssm_parameter.vpc_id](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/data-sources/ssm_parameter) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | AWS Region | `string` | `"us-east-1"` | no |
| <a name="input_env"></a> [env](#input\_env) | Environment | `string` | n/a | yes |
| <a name="input_project"></a> [project](#input\_project) | Project path | `string` | `"ta-lab/terraform/apps"` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Default tags for AWS provider | `map(string)` | <pre>{<br>  "DeleteAfter": "-",<br>  "Owner": "Crombal",<br>  "Temporary": false,<br>  "Terraform": "true"<br>}</pre> | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
