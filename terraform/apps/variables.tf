variable "env" {
  description = "Environment"
  type        = string
}

variable "aws_region" {
  description = "AWS Region"
  type        = string
  default     = "us-east-1"
}

variable "project" {
  description = "Project path"
  type        = string
  default     = "ta-lab/terraform/apps"
}

variable "tags" {
  description = "Default tags for AWS provider"
  type        = map(string)
  default = {
    Temporary   = false
    DeleteAfter = "-"
    Terraform   = "true"
    Owner       = "Crombal"
  }
}
