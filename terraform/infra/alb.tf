module "sg-alb" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "4.17.1"

  use_name_prefix = false
  name            = local.alb_name
  description     = "SG for ALB"
  vpc_id          = module.vpc.vpc_id

  ingress_with_cidr_blocks = [for rule in [
    {
      rule        = "http-80-tcp"
      cidr_blocks = "0.0.0.0/0"
      description = "TCP/HTTP on 80 port from all"
    }
  ] : rule if rule.cidr_blocks != ""]

  ingress_with_self = [{ rule = "all-all" }]

  egress_cidr_blocks = ["0.0.0.0/0"]
  egress_rules       = ["all-all"]

  tags = var.tags
}

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "6.5.0"

  name            = local.alb_name
  vpc_id          = module.vpc.vpc_id
  subnets         = module.vpc.public_subnets
  security_groups = [module.sg-alb.security_group_id]

  http_tcp_listeners = [{
    action_type = "fixed-response"
    fixed_response = {
      content_type = "text/plain"
      message_body = "Not found"
      status_code  = "404"
    }
    port               = 80
    protocol           = "HTTP"
    target_group_index = 0
  }]
}

resource "aws_route53_record" "confluent" {
  zone_id = aws_route53_zone.this.id
  name    = "alb"
  type    = "CNAME"
  ttl     = "30"
  records = [module.alb.lb_dns_name]
}
