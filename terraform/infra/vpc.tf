module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.19.0"

  name = local.vpc_name

  cidr = var.vpc_cidr
  azs  = var.vpc_azs

  private_subnets     = var.vpc_private_subnets
  private_subnet_tags = local.vpc_private_subnet_tags
  public_subnets      = var.vpc_public_subnets
  public_subnet_tags  = local.vpc_public_subnet_tags

  enable_dns_hostnames = true
}
