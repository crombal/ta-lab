module "nat" {
  source  = "int128/nat-instance/aws"
  version = "2.1.0"

  name = local.nat_instance_name

  vpc_id                      = module.vpc.vpc_id
  public_subnet               = module.vpc.public_subnets[0]
  private_subnets_cidr_blocks = module.vpc.private_subnets_cidr_blocks
  private_route_table_ids     = module.vpc.private_route_table_ids

  instance_types    = local.nat_instance_types
  use_spot_instance = false
  key_name          = local.key_pair_name

  tags = var.tags
}

resource "aws_eip" "nat" {
  network_interface = module.nat.eni_id
  tags              = local.nat_eip_tags
}

resource "aws_security_group_rule" "nat_ssh" {
  security_group_id = module.nat.sg_id
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
}
