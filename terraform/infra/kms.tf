data "aws_caller_identity" "current" {}

data "aws_ssm_parameter" "ansible_sops_iam_role_arns" {
  name            = "/ta-lab/ansible_sops_iam_role_arns"
  with_decryption = true
}

resource "aws_kms_key" "ansible_sops" {
  policy = data.aws_iam_policy_document.ansible_sops.json
}

resource "aws_kms_alias" "ansible_sops" {
  target_key_id = aws_kms_key.ansible_sops.id
  name          = "alias/ansible_sops"
}

data "aws_iam_policy_document" "ansible_sops" {
  statement {
    sid    = "Enable IAM User Permissions"
    effect = "Allow"
    principals {
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
      type        = "AWS"
    }
    actions = [
      "kms:*"
    ]
    resources = ["*"]
  }
  statement {
    sid    = "Allow access for Key Administrators"
    effect = "Allow"
    principals {
      identifiers = split(",", data.aws_ssm_parameter.ansible_sops_iam_role_arns.value)
      type        = "AWS"
    }
    actions = [
      "kms:Create*",
      "kms:Describe*",
      "kms:Enable*",
      "kms:List*",
      "kms:Put*",
      "kms:Update*",
      "kms:Revoke*",
      "kms:Disable*",
      "kms:Get*",
      "kms:Delete*",
      "kms:TagResource",
      "kms:UntagResource",
      "kms:ScheduleKeyDeletion",
      "kms:CancelKeyDeletion"
    ]
    resources = ["*"]
  }
  statement {
    sid    = "Allow use of the key"
    effect = "Allow"
    principals {
      identifiers = split(",", data.aws_ssm_parameter.ansible_sops_iam_role_arns.value)
      type        = "AWS"
    }
    actions = [
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey"
    ]
    resources = ["*"]
  }
  statement {
    sid    = "Allow attachment of persistent resources"
    effect = "Allow"
    principals {
      identifiers = split(",", data.aws_ssm_parameter.ansible_sops_iam_role_arns.value)
      type        = "AWS"
    }
    condition {
      test     = "Bool"
      values   = [true]
      variable = "kms:GrantIsForAWSResource"
    }
    actions = [
      "kms:CreateGrant",
      "kms:ListGrants",
      "kms:RevokeGrant"
    ]
    resources = ["*"]
  }
}
