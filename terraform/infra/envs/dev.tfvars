env                 = "dev"
vpc_azs             = ["us-east-1a", "us-east-1b", "us-east-1c"]
vpc_cidr            = "10.0.0.0/22"
vpc_private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
vpc_public_subnets  = ["10.0.0.0/28", "10.0.0.16/28", "10.0.0.32/28"]
