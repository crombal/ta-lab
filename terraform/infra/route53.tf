resource "aws_route53_zone" "this" {
  name = local.route53_zone_name
  vpc {
    vpc_id = module.vpc.vpc_id
  }
}