locals {
  name = "crombal"

  dynamodb_table_name = "terraform-lock"

  s3_bucket_name = "tf-state.${var.env}.${local.name}"

  vpc_name = "${local.name}-${var.env}"
  vpc_private_subnet_tags = {
    Type = "Private"
  }
  vpc_public_subnet_tags = {
    Type = "Public"
  }

  route53_zone_name = "${local.name}-${var.env}.com"

  alb_name = "${local.name}-${var.env}"

  nat_instance_name = "${local.name}-${var.env}"
  nat_eip_tags = {
    Name = local.nat_instance_name
  }
  nat_instance_types = ["t2.micro"]

  key_pair_name = local.name
}
