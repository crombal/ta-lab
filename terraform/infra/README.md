# infra

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.3, >= 1.3.7 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 4.52.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.52.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_alb"></a> [alb](#module\_alb) | terraform-aws-modules/alb/aws | 6.5.0 |
| <a name="module_nat"></a> [nat](#module\_nat) | int128/nat-instance/aws | 2.1.0 |
| <a name="module_s3_bucket"></a> [s3\_bucket](#module\_s3\_bucket) | terraform-aws-modules/s3-bucket/aws | 3.6.1 |
| <a name="module_sg-alb"></a> [sg-alb](#module\_sg-alb) | terraform-aws-modules/security-group/aws | 4.17.1 |
| <a name="module_vpc"></a> [vpc](#module\_vpc) | terraform-aws-modules/vpc/aws | 3.19.0 |

## Resources

| Name | Type |
|------|------|
| [aws_dynamodb_table.terraform_lock](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/dynamodb_table) | resource |
| [aws_eip.nat](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/eip) | resource |
| [aws_kms_alias.ansible_sops](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/kms_alias) | resource |
| [aws_kms_key.ansible_sops](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/kms_key) | resource |
| [aws_route53_record.confluent](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/route53_record) | resource |
| [aws_route53_zone.this](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/route53_zone) | resource |
| [aws_security_group_rule.nat_ssh](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/security_group_rule) | resource |
| [aws_ssm_parameter.alb_http_80_listener_arn](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.private_route_table_ids](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.private_subnet](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.private_subnets](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.private_subnets_cidr_blocks](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.public_subnet](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.public_subnets](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.public_subnets_cidr_blocks](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.route53_zone_id](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.vpc_id](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/resources/ssm_parameter) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.ansible_sops](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/data-sources/iam_policy_document) | data source |
| [aws_ssm_parameter.ansible_sops_iam_role_arns](https://registry.terraform.io/providers/hashicorp/aws/4.52.0/docs/data-sources/ssm_parameter) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | AWS Region | `string` | `"us-east-1"` | no |
| <a name="input_env"></a> [env](#input\_env) | Environment | `string` | n/a | yes |
| <a name="input_project"></a> [project](#input\_project) | Project path | `string` | `"ta-lab/terraform/infra"` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Default tags for AWS provider | `map(string)` | <pre>{<br>  "DeleteAfter": "-",<br>  "Owner": "Crombal",<br>  "Temporary": false,<br>  "Terraform": "true"<br>}</pre> | no |
| <a name="input_vpc_azs"></a> [vpc\_azs](#input\_vpc\_azs) | A list of availability zones names or ids in the region | `list(string)` | n/a | yes |
| <a name="input_vpc_cidr"></a> [vpc\_cidr](#input\_vpc\_cidr) | The IPv4 CIDR block for the VPC | `string` | n/a | yes |
| <a name="input_vpc_private_subnets"></a> [vpc\_private\_subnets](#input\_vpc\_private\_subnets) | A list of private subnets inside the VPC | `list(string)` | n/a | yes |
| <a name="input_vpc_public_subnets"></a> [vpc\_public\_subnets](#input\_vpc\_public\_subnets) | A list of public subnets inside the VPC | `list(string)` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
