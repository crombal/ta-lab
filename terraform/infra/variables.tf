variable "env" {
  description = "Environment"
  type        = string
}

variable "aws_region" {
  description = "AWS Region"
  type        = string
  default     = "us-east-1"
}

variable "project" {
  description = "Project path"
  type        = string
  default     = "ta-lab/terraform/infra"
}

variable "tags" {
  description = "Default tags for AWS provider"
  type        = map(string)
  default = {
    Temporary   = false
    DeleteAfter = "-"
    Terraform   = "true"
    Owner       = "Crombal"
  }
}

variable "vpc_azs" {
  description = "A list of availability zones names or ids in the region"
  type        = list(string)
}

variable "vpc_cidr" {
  description = "The IPv4 CIDR block for the VPC"
  type        = string
}

variable "vpc_private_subnets" {
  description = "A list of private subnets inside the VPC"
  type        = list(string)
}

variable "vpc_public_subnets" {
  description = "A list of public subnets inside the VPC"
  type        = list(string)
}
