### VPC ###

resource "aws_ssm_parameter" "vpc_id" {
  name  = "/ta-lab/terraform/infra/vpc_id"
  type  = "SecureString"
  value = module.vpc.vpc_id
}

### SUBNETS ###

resource "aws_ssm_parameter" "public_subnets" {
  name  = "/ta-lab/terraform/infra/public_subnets"
  type  = "SecureString"
  value = join(",", module.vpc.public_subnets)
}

resource "aws_ssm_parameter" "public_subnet" {
  name  = "/ta-lab/terraform/infra/public_subnet"
  type  = "SecureString"
  value = module.vpc.public_subnets[0]
}

resource "aws_ssm_parameter" "private_subnets" {
  name  = "/ta-lab/terraform/infra/private_subnets"
  type  = "SecureString"
  value = join(",", module.vpc.private_subnets)
}

resource "aws_ssm_parameter" "private_subnet" {
  name  = "/ta-lab/terraform/infra/private_subnet"
  type  = "SecureString"
  value = module.vpc.private_subnets[0]
}

resource "aws_ssm_parameter" "private_subnets_cidr_blocks" {
  name  = "/ta-lab/terraform/infra/private_subnets_cidr_blocks"
  type  = "SecureString"
  value = join(",", module.vpc.private_subnets_cidr_blocks)
}

resource "aws_ssm_parameter" "public_subnets_cidr_blocks" {
  name  = "/ta-lab/terraform/infra/public_subnets_cidr_blocks"
  type  = "SecureString"
  value = join(",", module.vpc.public_subnets_cidr_blocks)
}

### ROUTE TABLES ###

resource "aws_ssm_parameter" "private_route_table_ids" {
  name  = "/ta-lab/terraform/infra/private_route_table_ids"
  type  = "SecureString"
  value = join(",", module.vpc.private_route_table_ids)
}

### ROUTE53 ###

resource "aws_ssm_parameter" "route53_zone_id" {
  name  = "/ta-lab/terraform/infra/route53_zone_id"
  type  = "SecureString"
  value = aws_route53_zone.this.id
}

### ALB ###

resource "aws_ssm_parameter" "alb_http_80_listener_arn" {
  name  = "/ta-lab/terraform/infra/alb_http_80_listener_arn"
  type  = "SecureString"
  value = module.alb.http_tcp_listener_arns[0]
}
