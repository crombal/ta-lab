resource "aws_lb_target_group" "this" {
  for_each = {
    for idx, rule in var.target_rules : rule["port"] => rule if local.create && var.create_alb_rule
  }

  name_prefix = "${var.name}-"
  protocol    = "HTTP"
  port        = each.value.port
  vpc_id      = var.vpc_id
}

resource "aws_lb_target_group_attachment" "test" {
  for_each = {
    for idx, rule in var.target_rules : rule["port"] => rule if local.create && var.create_alb_rule
  }

  target_group_arn = aws_lb_target_group.this[each.value.port].arn
  target_id        = aws_instance.this[0].id
}

resource "aws_lb_listener_rule" "this" {
  for_each = {
    for idx, rule in var.target_rules : rule["port"] => rule if local.create && var.create_alb_rule
  }

  listener_arn = var.alb_listener_arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.this[each.value.port].arn
  }

  condition {
    path_pattern {
      values = each.value.paths
    }
  }
}
