resource "aws_route53_record" "this" {
  count = local.create && var.create_route_53_record ? 1 : 0

  zone_id = var.route53_zone_id
  name    = var.name
  type    = "A"
  ttl     = "300"
  records = [aws_instance.this[0].private_ip]
}
