module "sg" {
  count = local.create ? 1 : 0

  source  = "terraform-aws-modules/security-group/aws"
  version = "4.17.1"

  use_name_prefix = false
  name            = var.name
  description     = "SG for EC2"
  vpc_id          = var.vpc_id

  ingress_with_cidr_blocks = var.ingress_with_cidr_blocks

  ingress_with_self = [{ rule = "all-all" }]

  egress_cidr_blocks = ["0.0.0.0/0"]
  egress_rules       = ["all-all"]

  tags = var.tags
}
